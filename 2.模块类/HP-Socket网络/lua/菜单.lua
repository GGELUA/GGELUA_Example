--[[
    @Author       : GGELUA
    @Date         : 2021-04-25 11:02:57
    @LastEditTime : 2021-05-21 19:21:09
--]]
local _ENV = setmetatable({}, {__index=_G})
local 顶部菜单 = require("IMGUI.顶部菜单")()
local PACK模式 = require("IMGUI.菜单")("PACK模式")
local RPC模式 = require("IMGUI.菜单")("RPC模式")
local Server = require("IMGUI.菜单选项")("Server")
local Client = require("IMGUI.菜单选项")("Client")

function 显示()
    if 顶部菜单:开始() then
        if PACK模式:开始() then
            if Server:显示() then
                require("PACK服务端"):打开()
            end
            if Client:显示() then
                require("PACK客户端"):打开()
            end
            PACK模式:结束()
        end
        
        if RPC模式:开始() then
            if Server:显示() then
                require("RPC服务端"):打开()
            end
            if Client:显示() then
                require("RPC客户端"):打开()
            end
            RPC模式:结束()
        end
        顶部菜单:结束()
    end
end

return _ENV