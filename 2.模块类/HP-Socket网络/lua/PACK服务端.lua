--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 17:48:53
    @LastEditTime : 2021-05-08 02:04:23
--]]
local LOGS = {}
local function LOG(t,...)
    table.insert( LOGS,t:format(...) )
end
local 服务端 = require("HPS.PackServer")()

function 服务端:准备事件(...)
    LOG("[准备事件]->监听地址:%s:%d",self:取监听地址())
end

function 服务端:停止事件()
    LOG("[停止事件]")
end

function 服务端:连接事件(...)
    LOG("[连接事件]->ID:%s [%s:%s]",...)
end

function 服务端:断开事件(...)
    LOG("[断开事件]->ID:%s [%s:%s]",...)
end

function 服务端:接收事件(id,...)
    LOG("[接收事件]->ID:%s [%s]",id,...)
    self:发送(id,...)
end


local _ENV = setmetatable({}, {__index=_G})

local 窗口 = require("IMGUI.窗口")("PackServer")
local 地址 = require("IMGUI.输入")("地址","127.0.0.1"):置宽度(200)
local 端口 = require("IMGUI.输入")("端口",8888):置宽度(100)
local 启动 = require("IMGUI.复选按钮")("启动")

local 连接ID = require("IMGUI.输入")("ID",1):置宽度(100)
local 断开连接 = require("IMGUI.按钮")("断开连接")
local 区域 = require("IMGUI.区域")("LOG",0,400,true):自动滚动(true)

function 显示()
    if 窗口:开始() then
        地址:显示()
        端口:不换行():显示()
        if 启动:不换行():显示() then
            if 启动:是否选中() then
                服务端:启动(地址:取值(),端口:取值())
            else
                服务端:停止()
            end
        end
        
        连接ID:显示()
        if 断开连接:不换行():显示() then
            服务端:断开(连接ID:取值())
        end
        if 区域:开始() then
            for i,v in ipairs(LOGS) do
                区域:文本(v)
            end
            区域:结束()
        end

        窗口:结束()
    end
    return 窗口:是否打开()
end

function 打开()
    if not 窗口:是否打开() then
        窗口:打开()
        table.insert( __窗口列表, _ENV)
    end
end

return _ENV