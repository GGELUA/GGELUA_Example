--[[
    @Author       : GGELUA
    @Date         : 2019-06-23 12:24:23
    @LastEditTime : 2021-05-22 07:52:46
--]]

local SDL = require("SDL")
引擎 = require"SDL.窗口"{
    标题 = "GGELUA_HP-Socket",
    宽度 = 1280,
    高度 = 720,
    帧率 = 60,
    可调整 = true,
    渲染器 = 'opengl'
}

local im = require("IMGUI")()--:打开DEMO()
local 菜单 = require("菜单")
__窗口列表 = {}

print(require("hps.hpsocket").取版本())
require("RPC服务端"):打开()
require("RPC客户端"):打开()

function 引擎:更新事件(dt,x,y)
    im:开始()
        菜单:显示()
        for i,v in ipairs(__窗口列表) do
            if not v:显示() then
                table.remove( __窗口列表,i )
                break
            end
        end
    im:结束()
end

function 引擎:渲染事件(dt,x,y)
    self:渲染清除(0x70,0x70,0x70)
        im:显示()
    self:渲染结束()
end

function 引擎:窗口事件(消息)
    if 消息==SDL.窗口_关闭 then
        引擎:关闭()
    end
end

function 引擎:键盘事件(KEY,KMOD,状态,按住)
    if not 状态 then--弹起
        if KEY==SDL.KEY_F1 then
            print('F1')
        end
    end
    if KMOD&SDL.KMOD_LCTRL~=0 then
        print('左CTRL',按住)
    end
    if KMOD&SDL.KMOD_ALT~=0 then
        print('左右ALT',按住)
    end
end

function 引擎:鼠标事件(...)
    
end

function 引擎:输入事件(...)
    print(...)
end

function 引擎:销毁事件()

end
