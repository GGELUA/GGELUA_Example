--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 17:49:01
    @LastEditTime : 2021-05-22 12:37:40
--]]
local LOGS = {}
local function LOG(t,...)
    table.insert( LOGS,t:format(...) )
end

local 客户端,RPC = require("HPS.RPCClient")()

function 客户端:准备事件(...)
    LOG("[准备事件]")
end

function 客户端:连接事件(ID)
    LOG("[连接事件]->ID:%s [%s:%s]",ID,self:取本地地址())
end

function 客户端:断开事件(...)
    LOG("[断开事件]->ID:%s [%s:%s]",...)
end


local _ENV = setmetatable({}, {__index=_G})

local 窗口 = require("IMGUI.窗口")("RPCClient")
local 地址 = require("IMGUI.输入")("地址","127.0.0.1"):置宽度(200)
local 端口 = require("IMGUI.输入")("端口",8888):置宽度(100)
local 异步 = require("IMGUI.复选按钮")("异步",true)
local 连接 = require("IMGUI.按钮")("连接")

local TEST1 = require("IMGUI.按钮")("TEST1")
local 区域 = require("IMGUI.区域")("LOG",0,400,true):自动滚动(true)
local 输入窗口 = require("IMGUI.模态窗口")("输入")
local 多行输入 = require("IMGUI.多行输入")("多行输入",200,100,'内容')
local 回复按钮 = require("IMGUI.按钮")("回复")

function RPC:访问客户端(...)
    LOG("[访问客户端]->[%s]",table.concat( {...}, ", "))
    等待输入 = coroutine.running()
    return coroutine.yield()
end

function 输入窗口显示()
    if not 输入窗口:是否打开() then
        输入窗口:打开()
    end
    if 输入窗口:开始() then
        输入窗口:文本("服务端请求回复")
        多行输入:显示()
        if 回复按钮:显示() then
            coroutine.resume(等待输入,多行输入:取值())
            等待输入 = nil
            输入窗口:关闭()
        end
        输入窗口:结束()
    end
end

function 显示()
    if 窗口:开始() then
        地址:显示()
        端口:不换行():显示()
        异步:不换行():显示()
        if 异步:是否停留() then
            异步:提示('异步在连接的时候不会阻塞(卡),但是连接结果只能从事件返回.')
        end
        if 连接:不换行():显示() and not 客户端:是否启动() then
            客户端:连接(地址:取值(),端口:取值(),异步:是否选中())
        end

        if TEST1:显示() and 客户端:是否启动() then
            coroutine.wrap( function ()
                local r1,r2 = 客户端:访问服务端(1,2,3)--想要获得返回值，必须要协程里调用
                LOG("[返回事件]->%s %s",r1,r2)
            end )()
        end
        if 区域:开始() then
            for i,v in ipairs(LOGS) do
                区域:文本(v)
            end
            区域:结束()
        end

        窗口:结束()
    end
    if 等待输入 then
        输入窗口显示()
    end
    return 窗口:是否打开()
end

function 打开()
    if not 窗口:是否打开() then
        窗口:打开()
        窗口:置坐标(650,100)
        table.insert( __窗口列表, _ENV)
    end
end

return _ENV