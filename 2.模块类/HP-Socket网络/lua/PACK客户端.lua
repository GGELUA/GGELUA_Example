--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 17:49:01
    @LastEditTime : 2021-05-21 19:55:55
--]]
local LOGS = {}
local function LOG(t,...)
    table.insert( LOGS,t:format(...) )
end

local 客户端 = require("HPS.PackClient")()

function 客户端:准备事件(...)
    LOG("[准备事件]")
end

function 客户端:连接事件()
    LOG("[连接事件]->[%s:%s]",self:取本地地址())
end

function 客户端:断开事件(...)
    LOG("[断开事件]->[%s:%s]",...)
end

function 客户端:接收事件(...)
    LOG("[接收事件]->[%s]",...)
end

local _ENV = setmetatable({}, {__index=_G})

local 窗口 = require("IMGUI.窗口")("PackClient")
local 地址 = require("IMGUI.输入")("地址","127.0.0.1"):置宽度(200)
local 端口 = require("IMGUI.输入")("端口",8888):置宽度(100)
local 异步 = require("IMGUI.复选按钮")("异步",true)
local 连接 = require("IMGUI.按钮")("连接")

local 内容 = require("IMGUI.输入")("##内容","")
local 发送 = require("IMGUI.按钮")("发送")


local 区域 = require("IMGUI.区域")("LOG",0,400,true):自动滚动(true)


function 显示()
    if 窗口:开始() then
        地址:显示()
        端口:不换行():显示()
        异步:不换行():显示()
        if 异步:是否停留() then
            异步:提示('异步在连接的时候不会阻塞(卡),但是连接结果只能从事件返回.')
        end
        if 连接:不换行():显示() and not 客户端:是否启动() then
            客户端:连接(地址:取值(),端口:取值(),异步:是否选中())
        end
        内容:显示()
        if 发送:不换行():显示() and 客户端:是否启动() and 内容:取值()~='' then
            客户端:发送(内容:取值())
        end
        if 区域:开始() then
            for i,v in ipairs(LOGS) do
                区域:文本(v)
            end
            区域:结束()
        end

        窗口:结束()
    end
    return 窗口:是否打开()
end

function 打开()
    if not 窗口:是否打开() then
        窗口:打开()
        窗口:置坐标(600,60)
        table.insert( __窗口列表, _ENV)
    end
end

return _ENV